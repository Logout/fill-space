#include <stdio.h>
#include <string.h>

#define BLOCK_SIZE 1024
#define CHUNK_SIZE 50
#define FILENAME "filler.dat"

char zero_buffer[BLOCK_SIZE];

void main(argc,argv)
  int argc;
  char** argv;
{
    FILE* filler = NULL;
    unsigned long blocks_written, blocks_per_iteration;
    unsigned long chunk_count, remaining_blocks;
    int i, j;
    char confirm;

    if (argc < 2) {
        printf("Please supply a block count target.\n");
        exit(1);
    }
    memset(zero_buffer, 0, BLOCK_SIZE);
    remaining_blocks = atol(argv[1]);
    printf("Writing file: block_count=%u\n", remaining_blocks);
    /* division on the required numeric range seems to be broken,
       so I guess I am counting up... */
    for (i = 0; i < remaining_blocks; ) {
        ++chunk_count;
        i += CHUNK_SIZE;
    }
    printf("Writing file: chunk_count=%u\n", chunk_count);
    printf("OK?\n");
    read(0, &confirm, 1);
    if (confirm != 'y') {
        exit(1);
    }
    blocks_written = 0;
    blocks_per_iteration = CHUNK_SIZE;

    filler = fopen(FILENAME, "w");
    for (i = 0; i < chunk_count; ++i) {
        if (remaining_blocks < CHUNK_SIZE) {
            blocks_per_iteration = remaining_blocks;
        }
        for (j = 0; j < blocks_per_iteration; ++j) {
            fwrite(zero_buffer, BLOCK_SIZE, 1, filler);
        }
        blocks_written += blocks_per_iteration;
        remaining_blocks -= blocks_per_iteration;
        printf("Wrote %u blocks, ", blocks_written);
        printf("%u remaining\n", remaining_blocks);
    }
    printf("Done, closing file.\n");
    fclose(filler);
    printf("Deleting file...\n");
    unlink(FILENAME);
    printf("Done, your hard drive should be clean now. :)\n");
}

